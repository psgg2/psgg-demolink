// SLICK JS
$(document).ready(function(){
    $('.card-deck').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
      {
        breakpoint: 1199,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        }
      }
      ],
      prevArrow: "<div class='slick-nav position-absolute prev'><div class='position-absolute icon-container'><i class='fas fa-chevron-left'></i></div></div>",
      nextArrow: "<div class='slick-nav position-absolute next'><div class='position-absolute icon-container'><i class='fas fa-chevron-right'></i></div></div>",

    });
});

// Footer
$(window).ready(function(){
    let f_height = $('.footer').height() + 50;
    $('body').css('padding-bottom',f_height+'px');
});

// Magnific Popup
$(document).ready(function() {
    $('.popup-gallery').magnificPopup({
        delegate: 'span',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            // titleSrc: function(item) {
            //     return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
            // }
        }
    });
});


// PRE LOADER
$(window).load(function() {
    // hide loading gif when page done loading
    $(".loader").fadeOut("1000");
});


// $(document).ready(function(){
//     let owl = $('.owl-carousel');
//     owl.owlCarousel({
//         items: 1,
//         loop: true,
//         autoplay: false,
//         autoplayTimeout: 5000,
//         autoplayHoverPause:true,
//         nav: true,
//         navText: ['<div class="nav-circle"><i class="fa fa-chevron-left fa-2em"></i></div>', '<div class="nav-circle"><i class="fa fa-chevron-right fa-2em"></i></div>'],
//     });
// });

// let TxtRotate = function(el, toRotate, period) {
//     this.toRotate = toRotate;
//     this.el = el;
//     this.loopNum = 0;
//     this.period = parseInt(period, 10) || 2000;
//     this.txt = '';
//     this.tick();
//     this.isDeleting = false;
// };

// TxtRotate.prototype.tick = function() {
//     let i = this.loopNum % this.toRotate.length;
//     let fullTxt = this.toRotate[i];

//     if (this.isDeleting) {
//         this.txt = fullTxt.substring(0, this.txt.length - 1);
//     } else {
//         this.txt = fullTxt.substring(0, this.txt.length + 1);
//     }

//     this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

//     let that = this;
//     let delta = 300 - Math.random() * 100;

//     if (this.isDeleting) { delta /= 3; }

//     if (!this.isDeleting && this.txt === fullTxt) {
//         delta = this.period;
//         this.isDeleting = true;
//     } else if (this.isDeleting && this.txt === '') {
//         this.isDeleting = false;
//         this.loopNum++;
//         delta = 500;
//     }

//     setTimeout(function() {
//         that.tick();
//     }, delta);
// };

// window.onload = function() {
//     let elements = document.getElementsByClassName('txt-rotate');
//     for (let i=0; i<elements.length; i++) {
//         let toRotate = elements[i].getAttribute('data-rotate');
//         let period = elements[i].getAttribute('data-period');
//         if (toRotate) {
//             new TxtRotate(elements[i], JSON.parse(toRotate), period);
//         }
//     }
// // INJECT CSS
// let css = document.createElement("style");
// css.type = "text/css";
// css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
// document.body.appendChild(css);
// };


